import "../styles/globals.css";
import { AppProvider } from "../contexts/app.context";

function MyApp({ Component, pageProps }) {
  return (
    <AppProvider>
      <Component {...pageProps} />
    </AppProvider>
  );
}

export default MyApp;
